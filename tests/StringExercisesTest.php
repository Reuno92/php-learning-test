<?php

use Renauddev\PhpLearning\StringExercises;
use PHPUnit\Framework\TestCase;

class StringExercisesTest extends TestCase
{
    protected StringExercises $stringExercises;

    protected function setUp(): void
    {
        $this->stringExercises = new StringExercises();
    }

    /**
     * @test
     * @return void
     */
    public function getString(): void
    {
        self::assertEquals(
            'renaud',
            $this->stringExercises->getString('renaud')
        );
    }

    /**
     * @test
     * @return void
     */
    public function getStringInUppercase(): void
    {
        self::assertEquals(
            'RENAUD',
            $this->stringExercises->getStringInUpperCase('renaud')
        );
    }

    /**
     * @test
     * @return void
     */
    public function getStringInLowercase(): void
    {
        self::assertEquals(
        'renaud',
            $this->stringExercises->getStringInLowerCase('RENAUD')
        );
    }

    /**
     * @test
     * @return void
     */
    public function getStringTitled(): void
    {
        self::assertEquals(
            "I'm Big One",
            $this->stringExercises->getStringTitled("i'm big One"),
            'One letter at least is in uppercase when it not first letter'
        );

        self::assertEquals(
            "I'm Big One",
            $this->stringExercises->getStringTitled("i'M bIg ONE"),
            'One letter at least is in uppercase when it not first letter'
        );

        self::assertEquals(
            "I'm Big One",
            $this->stringExercises->getStringTitled("I'M BIG ONE"),
            'One letter at least is in uppercase when it not first letter'
        );

        self::assertEquals(
            "Renaud Racinet",
            $this->stringExercises->getStringTitled("renaud racinet"),
            'One letter at least is in uppercase when it not first letter.'
        );
    }

    /**
     * @test
     * @return void
     */
    public function WithFirstLetterOnly(): void
    {
        self::assertEquals(
            'Lambrusco',
            $this->stringExercises->getStringWithFirstLetterOnly('lambrusco'),
            'One letter at least is in uppercase when it not first letter.'
        );

        self::assertEquals(
            'Chianti',
            $this->stringExercises->getStringWithFirstLetterOnly('cHiAnTi'),
            'One letter at least is in uppercase when it not first letter.'
        );
    }

    /**
     * @test
     * @return void
     */
    public function setMorse(): void
    {
        self::assertEquals(
            '.',
            $this->stringExercises->setMorse('e'),
            "Return message isn't morse."
        );

        self::assertEquals(
            '.-',
            $this->stringExercises->setMorse('a'),
            "Return message isn't morse"
        );

        self::assertEquals(
            '.....',
            $this->stringExercises->setMorse('5'),
            "Return message isn't morse"
        );
    }
}
