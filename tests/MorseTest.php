<?php

use Renauddev\PhpLearning\lib\Morse;
use \PHPUnit\Framework\TestCase;
class MorseTest extends TestCase
{
    protected Morse $morseTest;

    protected function setUp(): void
    {
        $this->morseTest = new Morse();
    }

    /**
     * @test
     * @return void
     */
    public function setHumanMessageToMorseWithSimpleWord(): void
    {
        self::assertEquals(
            '.-. . -. .- ..- -..',
            $this->morseTest->getHumanMessageToMorse('renaud')
        );
    }

    /**
     * @test
     * @return void
     */
    public function setHumanMessageToMorseWithName(): void
    {
        self::assertEquals(
            '.-. . -. .- ..- -.. / .-. .- -.-. .. -. . -',
            $this->morseTest->getHumanMessageToMorse('renaud racinet')
        );
    }

    public function setHumanMessageToMorseWithSpecialChar(): void
    {
        self::assertEquals(
            '- .-. .-..- ...',
            $this->morseTest->getHumanMessageToMorse('très')
        );

        self::assertEquals(
            '. -. -.-. --- .-. . / - .-. .-..- ...',
            $this->morseTest->getHumanMessageToMorse('Encore très')
        );
    }

    /**
     * @test
     * @return void
     */
    public function setHumanMessageToMorseWithSentence(): void
    {
        self::assertEquals(
            '. -. -.-. --- .-. . / - .-. .-..- ... / .. .-. .-. .. - ..-.. . / .- .--. .-. .-..- ... / -. --- ... / -- .- -. .. --. .- -. -.-. . ... / ... . -..- ..- . .-.. .-.. . ... --..-- / ..- .-. ... ..- .-.. .. -. . / .-. ..-.. .. -- .--. .-.. --- .-. .- / .-- . -. -.. -.-- / -.. . / -.- .. -.. -. .- .--. .--. . .-. / --. ..-.. .-. .- .-. -.. / --- ..- / .... . .-. ...- ..-.. --..-- / ...- .. --- .-.. . ..- .-. ... / ..-. .- -. .- - .. --.- ..- . ... / . - / .-.. .. -... ..-.. .-. ..-.. ... --..-- / . -. / .--. .-. --- -- . - - .- -. - -....- .--- ..- .-. .- -. - / -... ..- - . .-. / -..- --..-- / -.-. . / -.-- .- -. -.- . . / --.. ..-.. .-.. ..-.. / --.- ..- .- -.. .-. .- --. ..-.. -. .- .. .-. . .-.-.-',
            $this->morseTest->getHumanMessageToMorse('Encore très irritée après nos manigances sexuelles, Ursuline réimplora Wendy de kidnapper Gérard ou Hervé, violeurs fanatiques et libérés, en promettant-jurant buter X, ce yankee zélé quadragénaire.')
        );
    }
}
