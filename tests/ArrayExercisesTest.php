<?php

use Renauddev\PhpLearning\ArrayExercises;
use \PHPUnit\Framework\TestCase;
class ArrayExercisesTest extends TestCase
{
    protected array $witnessArray = ["Apple", "Pear", "Banana"];

    /**
     * @test
     * @return void
     */
    public function arrayCount(): void
    {
        $array = new ArrayExercises($this->witnessArray);
        self::assertEquals(
            3,
            $array->countItems(),
            "Must return 3"
        );
    }

    /**
     * @test
     * @return int
     */
    public function addLastIndex(): void
    {
        $array = new ArrayExercises($this->witnessArray);
        self::assertEquals(
            [...$this->witnessArray, "Strawberry"],
            $array->addLastIndex("Strawberry"),
            "Strawberry isn't here !"
        );
    }

    /**
     * @test
     * @return void
     */
    public function removeLastIndex(): void
    {
        $array = new ArrayExercises([...$this->witnessArray, "Strawberry"]);
        self::assertEquals(
            $this->witnessArray,
            $array->removeLastIndex(),
            'Strawberry is here again!'
        );
    }

    /**
     * @test
     * @return void
     */
    public function addInFirstIndex(): void
    {
        $array = new ArrayExercises($this->witnessArray);
        self::assertEquals(
            ["Strawberry", ...$this->witnessArray],
            $array->addInFirstIndex("Strawberry"),
            'Strawberry is here again!'
        );
    }

    /**
     * @test
     * @return void
     */
    public function removeInFirstIndex(): void
    {
        $array = new ArrayExercises(["Strawberry", ...$this->witnessArray]);
        self::assertEquals(
            $this->witnessArray,
            $array->removeInFirstIndex(),
            'Must return fruits without Strawberry'
        );
    }

    /**
     * @test
     * @return void
     */
    public function sortItems(): void
    {
        $array = new ArrayExercises(["Strawberry", ...$this->witnessArray]);
        self::assertEquals(
            ["Apple", "Banana", "Pear", "Strawberry"],
            $array->sortItems(),
            'Must be sorted!'
        );
    }

    /**
     * @test
     * @return void
     */
    public function unSortItems(): void
    {
        $array = new ArrayExercises(["Strawberry", ...$this->witnessArray]);
        self::assertEquals(
            ["Strawberry", "Pear", "Banana", "Apple"],
            $array->unSortItems(),
            'Must be sorted by descending'
        );
    }
}
