<?php

use Renauddev\PhpLearning\MathExercises;

class MathExercisesTest extends \PHPUnit\Framework\TestCase
{


    /**
     * @test
     * @return void
     */
    public function add() : void
    {
        $math = new MathExercises();

        self::assertEquals(
            2,
            $math->addition(1,1),
            'Must to be int equals 2'
        );

        self::assertEquals(
            3,
            $math->addition(2,1),
            'Must to be int equals 3'
        );

        self::assertEquals(
            4,
            $math->addition(2,2),
            'Must to be int equals 4'
        );

        self::assertEquals(
            6,
            $math->addition(3,3),
            'Must to be int equals 6'
        );

        self::assertEquals(
            10,
            $math->addition(9,1),
            'Must to be int equals 10'
        );

        self::assertEquals(
            100,
            $math->addition(10,90),
            'Must to be int equals 10'
        );
    }

    /**
     * @test
     * @return void
     */
    public function sub(): void
    {
        $math = new MathExercises();

        self::assertEquals(
            0,
            $math->subtraction(1,1),
                "Must be int equals 0",
        );

        self::assertEquals(
            -1,
            $math->subtraction(0,1),
                "Must be int equals -1",
        );

        self::assertEquals(
            9,
            $math->subtraction(10,1),
                "Must be int equals 9",
        );

        self::assertEquals(
            19,
            $math->subtraction(72,53),
                "Must be int equals 19",
        );
    }

    /**
     * @test
     * @return void
     */
    public function divide(): void
    {
        $math = new MathExercises();

        self::assertEquals(
            2,
            $math->division(4,2),
            'Must be int equals 2'
        );

        self::assertEquals(
            16,
            $math->division(32,2),
            'Must be int equals 8'
        );
    }

    /**
     * @test
     * @return void
     */
    public function multiply(): void
    {
        $math = new MathExercises();

        self::assertEquals(
            4,
            $math->multiply(2,2),
            'Must be int equals 4'
        );

        self::assertEquals(
            8,
            $math->multiply(4,2),
            'Must be int equals 8'
        );

        self::assertEquals(
            32,
            $math->multiply(4,8),
            'Must be int equals 32'
        );
    }

    /**
     * @test
     * @return void
     */
    public function pow(): void
    {
        $math = new MathExercises();

        self::assertEquals(
            16,
            $math->power(4),
            'Must be int equals 16'
        );

        self::assertEquals(
            64,
            $math->power(8),
            'Must be int equals 64'
        );
    }

    /**
     * @test
     * @return void
     */
    public function square(): void
    {
        $math = new MathExercises();

        self::assertEquals(
            2,
            $math->square(4),
            'Must be int equals 4'
        );

        self::assertEquals(
            2.83,
            $math->square(8),
            'Must be int equals 2.83'
        );
    }

    /**
     * @test
     * @return void
     */
    public function pythagoras(): void
    {
        $math = new MathExercises();

        self::assertEquals(
            8,
            $math->pythagoras(2,2),
            'Must be int equals 8'
        );

        self::assertEquals(
            32,
            $math->pythagoras(4,4),
            'Must be int equals 32'
        );
    }
}
