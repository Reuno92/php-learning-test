<?php

use Renauddev\PhpLearning\TimeCodeExercises;
use PHPUnit\Framework\TestCase;

class TimecodeExercisesTest extends TestCase
{

    protected TimeCodeExercises $timeCode;

    protected function setUp(): void
    {
        $this->timeCode = new TimeCodeExercises(25);
    }

    /**
     * @test
     * @return void
     */
    public function verify(): void
    {
        $timeCodeTwentyFive = $this->timeCode;
        $timeMin = '00:00:00:00';
        $timeMaxTwentyFive = '23:59:59:24';
        $firstWrongTimeCodeTwentyFive = '23:59:59:25';

        $timeMaxThirty = '23:59:59:29';
        $firstWrongTimeCodeThirty = '23:59:59:30';

        $timeMaxFifty = '23:59:59:49';
        $firstWrongTimeCodeFifty = '23:59:59:50';

        $timeMaxSixty = '23:59:59:59';
        $firstWrongTimeCodeSixty = '23:59:59:60';

        self::assertEquals(
            true,
            $timeCodeTwentyFive->verifyTimeCode($timeMin),
            'Must be true',
        );

        self::assertEquals(
            true,
            $timeCodeTwentyFive->verifyTimeCode($timeMaxTwentyFive),
            'Must be true',
        );

        self::assertEquals(
            false,
            $timeCodeTwentyFive->verifyTimeCode($firstWrongTimeCodeTwentyFive),
            'Must be false, frames can\'t be equals to frame count. It must be frame count - 1 = 24.'
        );

        $timeCodeThirty = new TimeCodeExercises(30);
        self::assertEquals(
            true,
            $timeCodeThirty->verifyTimeCode($timeMin),
            'Must be true',
        );

        self::assertEquals(
            true,
            $timeCodeThirty->verifyTimeCode($timeMaxThirty),
            'Must be true',
        );

        self::assertEquals(
            false,
            $timeCodeThirty->verifyTimeCode($firstWrongTimeCodeThirty),
            'Must be false,  frames can\'t be equals to frame count. It must be frame count - 1 = 29.'
        );

        $timeCodeFifty = new TimeCodeExercises(50);
        self::assertEquals(
            true,
            $timeCodeFifty->verifyTimeCode($timeMin),
            'Must be true',
        );

        self::assertEquals(
            true,
            $timeCodeFifty->verifyTimeCode($timeMaxFifty),
            'Must be true',
        );

        self::assertEquals(
            false,
            $timeCodeFifty->verifyTimeCode($firstWrongTimeCodeFifty),
            'Must be false,  frames can\'t be equals to frame count. It must be frame count - 1 = 49.'
        );

        $timeCodeSixty = new TimeCodeExercises(60);
        self::assertEquals(
            true,
            $timeCodeSixty->verifyTimeCode($timeMin),
            'Must be true'
        );

        self::assertEquals(
            true,
            $timeCodeSixty->verifyTimeCode($timeMaxSixty),
            'Must be true'
        );

        self::assertEquals(
            false,
            $timeCodeSixty->verifyTimeCode($firstWrongTimeCodeSixty),
            'Must be false,  frames can\'t be equals to frame count. It must be frame count - 1 = 59.'
        );
    }

    public function VerifyTimeCodeWithMissingCharacter(): void
    {
        $firstSample = "0:00:00:00";
        $secondSample = "00:0:00:00";
        $thirthSample = '00:00:0:00';
        $fourthSample = '00:00:00:0';
        $fivethSample = '00::00:00';

        self::assertEquals(
            false,
            $this->timeCode->verifyTimeCode($firstSample),
            'Must be false'
        );

        self::assertEquals(
            false,
            $this->timeCode->verifyTimeCode($secondSample),
            'Must be false'
        );

        self::assertEquals(
            false,
            $this->timeCode->verifyTimeCode($thirthSample),
            'Must be false'
        );

        self::assertEquals(
            false,
            $this->timeCode->verifyTimeCode($fourthSample),
            'Must be false'
        );

        self::assertEquals(
            false,
            $this->timeCode->verifyTimeCode($fivethSample),
            'Must be false'
        );
    }

    /**
     * @test
     * @return void
     */
    public function convertSecondToTimecode(): void
    {
        self::assertEquals(
            '00:00:01:00',
            $this->timeCode->convertSecondToTimecode(1),
            'Must be equals to 00:00:01:00'
        );

        self::assertEquals(
            '00:01:00:00',
            $this->timeCode->convertSecondToTimecode(60),
            'Must be equals to 00:01:00:00'
        );

        self::assertEquals(
            '00:02:00:00',
            $this->timeCode->convertSecondToTimecode(120),
            'Must be equals to 00:02:00:00'
        );

        self::assertEquals(
            '01:00:00:00',
            $this->timeCode->convertSecondToTimecode(3600),
            'Must be equals to 01:00:00:00'
        );
    }
}
