# PHP Learning by testing

Write test and resolve it.

# Installation

## Install dependencies 
```bash
composer install
```

# Dependencies

| Name     | version | Description                                                                                                                          |
|:---------|:--------|:-------------------------------------------------------------------------------------------------------------------------------------|
| PHPunit  | 10.0.5  | PHPUnit is a programmer-oriented testing framework for PHP. It is an instance of the xUnit architecture for unit testing frameworks. |

# Class

| Name                       | Description                                          | Test Passed                                                                                                 |
|:---------------------------|:-----------------------------------------------------|:------------------------------------------------------------------------------------------------------------|
| StringExercises            | Strings manipulation                                 | - [x] Uppercase<br> - [x] Lowercase<br> - [x] Titlecase<br> - [x] FirstLetter                               |
| ArrayExercises             | Array manipulation                                   | - [x] Add<br> - [x] remove<br> - [x]  and manipulate                                                        |
| MathExercises              | Math manipulation                                    | [x] Operation                                                                                               |
| TimecodeExercises          | Application of different string and int manipulation | - [x] Verify timecode<br/> - [x] Conversion Second To TimeCode<br> - [x] Converision framecount to Timecode |
| RegualrExpressionExercises | Regualr Expresssion manipulation                     | - [ ] Substitution<br> - [ ] Replace <br> - [ ] Split                                                       |
| DateExercises              | Date manipulation                                    | - [ ] with timstamp<br>- [ ] with timezone<br>                                                              |
| FileExercises              | File manipulation                                    | - [ ] Open<br> - [ ] Read<br> – [ ] Create<br> - [ ] Update<br>                                             |
| XMLExercices               | XML Create and Parsing                               | - [ ] Decode<br> - [ ] Encode                                                                               |
| JSONExercies               | JSON Decode and encode                               | -[ ] Decode<br> - [ ] Encode                                                                                |
