<?php

namespace Renauddev\PhpLearning;

/**
 * @class
 * @description Class Exercises indexed and associative array.
 */
class ArrayExercises
{
    public array $array;
    public function __construct(
        public array $givenData
    )
    {
        $this->array = $this->givenData;
    }

    public function countItems(): int
    {
        return count($this->array);
    }

    public function addLastIndex($itemAtAdd): array
    {
        $this->array[] = $itemAtAdd;
        return $this->array;
    }

    public function removeLastIndex(): array
    {
        array_pop($this->array);
        return $this->array;
    }

    public function addInFirstIndex(string $item): array
    {
        array_unshift($this->array, $item);
        return $this->array;
    }

    public function removeInFirstIndex(): array
    {
        array_shift($this->array);
        return $this->array;
    }

    public function sortItems(): array
    {
        sort($this->array);
        return $this->array;
    }

    public function unSortItems(): array
    {
        rsort($this->array);
        return $this->array;
    }
}
