<?php
namespace Renauddev\PhpLearning;
use Renauddev\PhpLearning\lib\Morse;

class StringExercises
{

    public function __construct()
    {
    }

    public function getString(string $name): string
    {
        return $name;
    }

    public function getStringInUpperCase(string $str): string
    {
        return $this->upperCase($str);
    }

    public function getStringInLowerCase(string $str): string
    {
        return $this->lowerCase($str);
    }

    public function getStringTitled(string $str): string
    {
        return $this->titlecase($str);
    }

    public function getStringWithFirstLetterOnly(string $str): string
    {
        return $this->firstLetter($str);
    }

    public function setMorse(string $str): string
    {
        $morse = new Morse();
        return $morse->getHumanMessageToMorse($str);
    }

    private function upperCase(string $str): string
    {
        return strtoupper($this->getString($str));
    }

    private function lowerCase(string $str): string
    {
        return strtolower($this->getString($str));
    }

    private function titleCase(string $str): string
    {
        return ucwords($this->lowerCase($this->getString($str)));
    }

    private function firstLetter(string $str): string
    {
        return ucfirst($this->lowerCase($this->getString($str)));
    }
}
