<?php

namespace Renauddev\PhpLearning\lib;

/**
 * @class
 * @description Morse class for English and French language only.
 */
class Morse {
    private array $truthTable = [
        "a" => ".-",
        "à" => ".--.-",
        "b" => "-...",
        "c" => "-.-.",
        "d" => "-..",
        "e" => ".",
        "è" => ".-..-",
        "é" => "..-..",
        "f" => "..-.",
        "g" => "--.",
        "h" => "....",
        "i" => "..",
        "j" => ".---",
        "k" => "-.-",
        "l" => ".-..",
        "m" => "--",
        "n" => "-.",
        "o" => "---",
        "p" => ".--.",
        "q" => "--.-",
        "r" => ".-.",
        "s" => "...",
        "t" => "-",
        "u" => "..-",
        "ü" => "..--",
        "v" => "...-",
        "w" => ".--",
        "x" => "-..-",
        "y" => "-.--",
        "z" => "--..",
        "1" => ".----",
        "2" => "..---",
        "3" => "...--",
        "4" => "....-",
        "5" => ".....",
        "6" => "-....",
        "7" => "--...",
        "8" => "---..",
        "9" => "----.",
        "0" => "-----",
        "." => ".-.-.-",
        "," => "--..--",
        "?" => "..--..",
        "'" => ".----.",
        "!" => "-.-.--",
        "/" => "-..-.",
        "(" => "-.--.",
        ")" => "-.--.-",
        "&" => ".-...",
        ":" => "---...",
        ";" => "-.-.-.",
        "=" => "-...-",
        "+" => ".-.-.",
        "-" => "-....-",
        "_" => "..--.-",
        "\"" => ".-..-.",
        "$" => "...-..-",
        "@" => ".--.-.",
        " " => "/",
    ];

    /**
     * @param string $str - Human readable message
     * @return string - Morse
     */
    public function getHumanMessageToMorse(string $str): string
    {
        $toLowerCase = strtolower($str);
        $subString = preg_split('/(?<!^)(?!$)/u', $toLowerCase);
        $array_result = array();

        for ($i = 0; $i < count($subString); $i++)
        {
            $char = $subString[$i];
            $array_result[] = $this->truthTable[$char];
        }

        return implode(' ', $array_result);
    }
}
