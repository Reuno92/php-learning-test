<?php

namespace Renauddev\PhpLearning;

/**
 * Time Code Exercises
 * @description Permit to verify and convert time code
 * @class
 */
class TimecodeExercises
{
    private string $frameCount;
    private array $limitFrameCount;
    private int $initFrameRate;

    public function __construct(int $initFrameRate)
    {
        $this->initFrameRate = $initFrameRate;
        $this->frameCount = (string)$initFrameRate;
        $this->limitFrameCount = str_split($this->frameCount);
    }

    /**
     * Getter to verify time code.
     * @param string $time
     * @return bool
     */
    public function verifyTimeCode(string &$time): bool
    {
        return $this->isTimeCode($time);
    }

    public function convertSecondToTimecode(int $time): string
    {
        return $this->convertTimeCode($time);
    }

    /**
     * Give if the input is time code.
     * @param string $time
     * @return bool
     */
    private function isTimeCode(string $time): bool
    {
        $unitTen = $this->limitFrameCount[1] == 0 ? $this->limitFrameCount[0] - 1 : $this->limitFrameCount[0] ;
        $unit = $this->limitFrameCount[1] == 0 ? 9 : $this->limitFrameCount[1] - 1;

        $regEx = '/^([0-1][0-9]|20|21|22|23):([0-5][0-9]:){2}([0-1][0-9]|' .$unitTen . '[0-' . $unit  . ']' . ')$/';
        return (bool)preg_match_all($regEx, $time);
    }

    private function conversionSecond(int $time, string $unit): int
    {
        switch($unit) {
            case "timecode":
                return $time / $this->initFrameRate;
            case "minute":
                return $time * 60 * $this->initFrameRate;
            case "hour":
                return $time * 3600 * $this->initFrameRate;
            default:
                die("Time unit is not recognized");
        }
    }

    /**
     * Convert in time code
     * @param int $time - input time in second. please use conversionFrameCount();
     * @return string
     */
    private function convertTimeCode(int $time): string
    {
        $frames  = str_pad( (string)floor(($time % 1) *  $this->initFrameRate), 2, "0", STR_PAD_LEFT);
        $seconds = str_pad( (string)floor($time % 60), 2, "0", STR_PAD_LEFT);
        $minutes = str_pad( (string)floor(( $time / 60 ) % 60 ), 2, "0", STR_PAD_LEFT);
        $hours   = str_pad( (string)floor($time / 3600), 2, "0", STR_PAD_LEFT);

        return "{$hours}:{$minutes}:{$seconds}:{$frames}";
    }
}
