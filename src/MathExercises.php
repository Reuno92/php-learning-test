<?php

namespace Renauddev\PhpLearning;

class MathExercises
{

    public function __construct()
    {
    }

    public function addition(int $first, int $second): int
    {
        return $first + $second;
    }

    public function subtraction(int $first, int $second): int
    {
        return $first - $second;
    }

    public function division(int $first, int $second): int
    {
        return $first / $second;
    }

    public function multiply(int $first, int $second): int
    {
        return $first * $second;
    }

    public function power(int $number): int
    {
        return $number ** 2;
    }

    public function square(float $number): float
    {
        return number_format(sqrt($number), 2, '.', '');
    }

    public function pythagoras(int $a, int $b): int
    {
        return $a ** 2 + $b ** 2;
    }


}
